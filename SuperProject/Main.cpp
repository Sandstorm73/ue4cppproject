#include <iostream>

class Pet
{
public:
	Pet() {};
	virtual void Voice()
	{
		std::cout << "Some voice!" << std::endl;
	}
};

class Dog : public Pet
{
public:
	void Voice() override
	{
		std::cout << "Woof!" << std::endl;
	}
};

class Cat : public Pet
{
public:
	void Voice() override
	{
		std::cout << "Meow!" << std::endl;
	}
};

class Bird : public Pet
{
public:
	void Voice() override
	{
		std::cout << "Tweet!" << std::endl;
	}
};
int main()
{
	Pet* pets[20];
	int count = 0;
	for (Pet* &pet : pets) {
		if (count % 3 == 0) {
			pet = new Dog();
		}
		else if (count % 3 == 1) {
			pet = new Cat();
		}
		else if (count % 3 == 2) {
			pet = new Bird();
		}
		count++;
	}

	for (Pet* pet : pets) {
		pet->Voice();
	}
}